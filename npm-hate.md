In response to http://www.jongleberry.com/why-i-hate-npm.html. Some valid points raised, but lost in a sea of nonsense.

## Downtime and errors
* Haven't experienced this. At all.

## Slow
* npm is the fastest package manager I've ever personally used. I'm not aware of any consistently faster ones.
* How long `npm install` takes for an already installed application is not a relevant metric - that's not a real-world scenario.
* Component, by its own admission, downloads selectively. Of course it will be faster. Does he have any explanation as to *why* Component is faster?

## Unnecessary with GitHub
* Complete nonsense. Version management and repository hosting has absolutely nothing to do with shipping packaged software, and shouldn't. They are two fundamentally different things, and it's a terrible idea to have core functionality rely on using a service that is out of your control, for something it wasn't intended to be used for. That's practically guaranteed to go horribly wrong at some point.
* Requiring from a URL is an amazingly stupid idea. It brings all the library availability issues and security issues from the frontend, to the backend.

## Unnecessary publish step
* Again, complete nonsense. Again, version management != shipping packaged software. Fundamentally different things, and the package manager shouldn't cause git to be a dependency for normal usage. If it *really* bothers you that you need to run `npm publish`, set up a hook. Problem solved.
* An extra account is hardly relevant. In the larger scheme of things - people having tens to hundreds of accounts - that one account is not going to matter.

## No organizations
* Valid point.
* OAuth quickly becomes a mess in practice. See also PyPI.

## Annoying
* Valid point. Additionally, real error reporting is horribly bad - lots of boilerplate, not obvious what the actual error is.

## Monopoly
* Don't see how this is an issue. What's keeping you from rolling/using your own? require() != npm, they're different things.

## Nested dependencies
* Yes, it can be done better. However, the current situation is better than almost every other language package manager currently in existence - they cause version conflicts on a regular basis.
* Windows being unable to handle deeply nested paths is a Windows problem. It's 2015, come on. It's not the responsibility of Node or npm to solve this.

## Global namespaces
* Possibly a problem. Plenty of sane, non-arbitrary names, though - I strongly doubt that the arbitary names are caused by it being a global namespace, it's more likely just a developer/ecosystem preference. PyPI has a global namespace, and doesn't have a lot these kind of names either. Basically, the names are a separate problem, unrelated to npm.
* Author or organization doesn't give *that* much extra information, it mostly just encourages cargo cult behaviour. Fixing the npmjs search to return reasonable results is a much higher priority, as far as I'm concerned.

## Local modules
* Defining local modules is easily solved with something like `rfr`. Not a real problem.
* "Size of package.json" isn't a relevant metric. It's simply not meaningful. As far as I can tell, this is just trying to find something to pick on.